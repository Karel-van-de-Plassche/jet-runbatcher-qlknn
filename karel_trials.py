import os
import stat
import pwd
import shutil
import re
import datetime
from itertools import product
from collections import OrderedDict

import numpy as np
from IPython import embed

def prep_dir(run_dir):
    foverwrite = True
    if not foverwrite and os.path.isdir(run_dir):
        raise Exception('foverwrite = {!s}, refusing to overwrite {!s}'.format(foverwrite, run_dir))
    elif os.path.isdir(run_dir):
        shutil.rmtree(run_dir)
    os.mkdir(run_dir)


def create_ex(ref_dir, run_dir):
    ref_ex_path = os.path.join(ref_dir, ex_filename)
    if os.path.isfile(ref_ex_path):
        shutil.copy2(ref_ex_path,run_dir)
    else:
        print('Warning! No exfile found!')

def create_ext(ref_dir, run_dir):
    ref_ext_path = os.path.join(ref_dir, ext_filename)
    if os.path.isfile(ref_ext_path):
        shutil.copy2(ref_ext_path,run_dir)
    else:
        print('Warning! No extfile found!')

def copy_file(ref_dir, run_dir, file_name):
    ref_path = os.path.join(ref_dir, file_name)
    if os.path.isfile(ref_path):
        shutil.copy2(ref_path,run_dir)
    else:
        print('Warning! No {!s} found!'.format(file_name))

def create_llcmd(ref_dir, run_dir, jetto_bin_name, jetto_bin_userid, num_processors=2):
    with open(os.path.join(ref_dir, llcmd_name),'r') as origfile:
        with open(os.path.join(run_dir, llcmd_name),'w') as newfile:
            for line in origfile:
                if re.search(r'\s+job_name\s+=',line):
                    newline = "# @ %-12s = %s\n" % ("job_name",'jetto.'+run_name)
                elif re.search(r'\s+min_processors\s+=',line):
                    newline = "# @ %-12s = %s\n" % ("min_processors", num_processors)
                elif re.search(r'\s+max_processors\s+=',line):
                    newline = "# @ %-12s = %s\n" % ("max_processors", num_processors)
                elif re.search(r'\s+arguments\s+=',line):
                    newline = "# @ %-12s =  " % ("arguments")
                    mm = re.search(r'=\s+(.+)$',line)
                    sline = mm.group(1).split()
                    opts = OrderedDict()
                    for blob in sline:
                        if blob.startswith('-'):
                            opt = blob[1]
                            val = blob[2:]
                            if opt == 'x' and 'x' not in opts:
                                opts[opt] = []
                            if opt == 'x':
                                opts[opt].append(val)
                            else:
                                opts[opt] = val
                    opts.pop('e', None) #Do not send emails

                    if 'x' in opts and '64' not in opts['x']:
                        opts['x'].append('64')
                    #args are forcibly overwritten
                    #args = [word for word in sline if not word.startswith('-')]
                    #run = args[0]
                    #if len(args) > 1:
                    #    #defaults to repository version name if not set
                    #    version = args[1]
                    #else:
                    #    version = None
                    #if len(args) > 2:
                    #    # defaults to repository owner if not set
                    #    owner = args[2]
                    #else:
                    #    owner = None

                    args = [
                        run_name,
                        jetto_bin_name,
                        jetto_bin_userid
                    ]

                    for opt, vals in opts.items():
                        if opt != 'x':
                            vals = [vals]
                        for val in vals:
                            newline += '-{!s}{!s} '.format(opt, val)
                    for arg in args:
                        newline += '{!s} '.format(arg)
                    newline += '\n'
                elif re.search(r'\s+executable\s+=',line):
                    newline = "# @ %-12s = %s\n" % ("executable", os.path.join(run_dir, 'rjettov'))
                elif re.search(r'\s+initialdir\s+=',line):
                    newline = "# @ %-12s = %s\n" % ("initialdir",run_dir)
                else:
                    newline = line
                newfile.write(newline)

def create_jetto_in(ref_dir, run_dir, now, set_namelist_switches=None):
    if set_namelist_switches is None:
        set_namelist_switches = {}
    jettoin_name = 'jetto.in'
    written_to_file = []
    qlk_flags_in_ref_jettoin = []
    last_qlk_line = ''
    with open(os.path.join(ref_dir, jettoin_name),'r') as origfile:
        for line in origfile:
            if re.search('qlk_', line):
                splitted = re.split('\s+(.*)\s*=\s*(.*),', line)
                switch_name = splitted[1]
                switch_value = splitted[2].strip()
                qlk_flags_in_ref_jettoin.append(switch_name)
                last_qlk_line = line

    with open(os.path.join(ref_dir, jettoin_name),'r') as origfile:
        newlines = []
        for line in origfile:
            newline = None
            if re.search(r'^Date\s+:',line):
                newline = "%-30s : %s\n" % ("Date",now.strftime("%d/%m/%Y"))
            elif re.search(r'^Time\s+:',line):
                newline = "%-30s : %s\n" % ("Time",now.strftime("%H:%M:%S"))
            else:
                for switch_name, switch_value in set_namelist_switches.items():
                    if re.search(switch_name, line):
                        newline = '  {:s}=  {!s},\n'.format(switch_name, switch_value)
                        written_to_file.append(switch_name)
            if newline is None:
                newline = line
            newlines.append(newline)

        for switch_name in set_namelist_switches:
            if switch_name not in written_to_file:
                switch_value = set_namelist_switches[switch_name]
                if switch_name.startswith('qlk_'):
                    print('inserting', switch_name)
                    newline = '  {:s}=  {!s},\n'.format(switch_name, switch_value)
                    newlines.insert(newlines.index(last_qlk_line)+1, newline)
                else:
                    raise NotImplementedError('Putting {!s} in jetto.in'.format(switch_name))

        with open(os.path.join(run_dir, jettoin_name),'w') as newfile:
            for line in newlines:
                newfile.write(line)

def create_jetto_jset(ref_dir, run_dir, now, ref_ex_path, jetto_bin_name, jetto_bin_userid, set_namelist_switches=None):
    if set_namelist_switches is None:
        set_namelist_switches = {}
    set_namelist_indices = {}
    run_name = os.path.basename(run_dir)
    #max_cell_index = 0
    extra_namelist_rows = 0
    with open(os.path.join(ref_dir, jset_name),'r') as origfile:
        for line in origfile:
            if re.search('OutputExtraNamelist.selItems.cell', line):
                splitted = re.split('.*\[(\d+)\]\[(\d+)\]\s+:\s(.*)', line)
                cell_index  = int(splitted[1])
                cell_array_index = int(splitted[2])
                cell_value = splitted[3]
                #if cell_index > max_cell_index:
                #    max_cell_index = cell_index
                for switch_name in set_namelist_switches:
                    if re.search(switch_name, line):
                        if cell_value == switch_name and switch_name not in set_namelist_indices:
                            set_namelist_indices[switch_name] = cell_index
            elif re.search('^OutputExtraNamelist.selItems.rows', line):
                splitted = re.split(':\s(.*)', line)
                extra_namelist_rows = int(splitted[1])

    counter = 0
    for switch_name in set_namelist_switches:
        if switch_name not in set_namelist_indices:
            set_namelist_indices[switch_name] = extra_namelist_rows + counter
            counter += 1
    new_amount_rows = extra_namelist_rows + counter

    written_to_file = []
    with open(os.path.join(ref_dir, jset_name),'r') as origfile:
        orig_lines = origfile.readlines()

    with open(os.path.join(run_dir, jset_name),'w') as newfile:
        for cur_line_num, line in enumerate(orig_lines):
            newline = None
            if re.search(r'EOF',line):
                newline = ''
            elif re.search(r'\*',line):
                if orig_lines[cur_line_num + 1] == '*EOF\n':
                    newline = ''
            elif re.search(r'^Creation Name\s+:',line):
                newline = "%-59s : %s\n" % ("Creation Name",run_dir+'/jetto.jset')
            elif re.search(r'^Creation Date\s+:',line):
                newline = "%-59s : %s\n" % ("Creation Date",now.strftime("%d/%m/%Y"))
            elif re.search(r'^Creation Time\s+:',line):
                newline = "%-59s : %s\n" % ("Creation Time",now.strftime("%H:%M:%S"))
            elif re.search(r'^JobProcessingPanel\.runDirNumber\s+:',line):
                newline = "%-59s : %s\n" % ("JobProcessingPanel.runDirNumber", re.sub('^' + run_prefix, '', run_name))
            elif re.search(r'^SetUpPanel\.exFileName\s+:',line):
                newline = line
                #newline = "%-59s : %s\n" % ("SetUpPanel.exFileName",ref_ex_path)
            elif re.search(r'^JobProcessingPanel\.name\s+:',line):
                newline = "%-59s : %s\n" % ("JobProcessingPanel.name",jetto_bin_name)
            elif re.search(r'^JobProcessingPanel\.userid\s+:',line):
                newline = "%-59s : %s\n" % ("JobProcessingPanel.userid",jetto_bin_userid)
            elif re.search(r'^JobProcessingPanel\.numProcessors\s+:',line):
                newline = "%-59s : %s\n" % ("JobProcessingPanel.numProcessors",num_processors)
            elif re.search('^OutputExtraNamelist.selItems.cell', line):
                splt_str = r'^OutputExtraNamelist.selItems.cell\[(\d+)\]'
                splitted = re.split(splt_str, line)
                if len(splitted) > 2:
                    this_index = int(splitted[1])
                    if this_index > extra_namelist_rows - 1:
                        newline = ''
                    else:
                        for switch_name, switch_value in set_namelist_switches.items():
                            switch_index = set_namelist_indices[switch_name]
                            src_str = r'^OutputExtraNamelist.selItems.cell\[{!s}\]\[2\]\s+:'.format(switch_index)
                            if re.search(src_str, line):
                                print('Putting', switch_name)
                                newline = '{:<57} : {!s}\n'.format('OutputExtraNamelist.selItems.cell[{!s}][2]'.format(switch_index), switch_value)
                                written_to_file.append(switch_name)
            elif re.search('^OutputExtraNamelist.selItems.rows', line):
                newline = '{:<59} : {!s}\n'.format('OutputExtraNamelist.selItems.rows', new_amount_rows)
            if newline is None:
                newline = line
            newfile.write(newline)

        for switch_name in set_namelist_switches:
            if switch_name not in written_to_file:
                switch_value = set_namelist_switches[switch_name]
                switch_index = set_namelist_indices[switch_name]
                newline = '{:<57} : {!s}\n'.format('OutputExtraNamelist.selItems.cell[{!s}][0]'.format(switch_index), switch_name)
                newfile.write(newline)
                newline = '{:<57} : {!s}\n'.format('OutputExtraNamelist.selItems.cell[{!s}][1]'.format(switch_index), '')
                newfile.write(newline)
                newline = '{:<57} : {!s}\n'.format('OutputExtraNamelist.selItems.cell[{!s}][2]'.format(switch_index), switch_value)
                newfile.write(newline)
        newfile.write('*\n')
        newfile.write('*EOF\n')

run_prefix = 'run'
ex_filename = 'jetto.ex'
ext_filename = 'jetto.ext'
sin_filename = 'jetto.sin'
sgrid_filename = 'jetto.sgrid'
rjettov_name = 'rjettov'
llcmd_name = '.llcmd'
jset_name = 'jetto.jset'
if __name__ == '__main__':
    num_processors = 2

    set_namelist_switches_combo = OrderedDict([
        ('qlk_usenn', [1]),
        # ('qlk_NN_rot_switch', [0, 1, 2]), Removed in post-alpha
        ('qlk_rot_flag', [0, 1, 2]),
        #('qlk_NN_alpha', [0, 1]), # Removed in monster commit
        #('qlk_NN_TEM', [0, 1]),
        ('qlk_NN_TEM', [1]),
        #('qlk_NN_part_trans_switch', [1, 2, 3]),
        ('qlk_NN_part_trans_switch', [1]),
    ])

    tiny_name = OrderedDict([
        ('qlk_rot_flag', 'r'),
        ('qlk_NN_TEM', 't'),
        ('qlk_NN_part_trans_switch', 'p'),
    ])

    user_name = pwd.getpwuid(os.getuid())[0]
    runs_dir = '/common/cmg/{!s}/jetto/runs/'.format(user_name)

    for ref_user, ref_run_name in [
            ('kplass', 'run92436_s19s005_0'), # 92436
            #('jcitrin', 'run92398_pred_s1'), # 92398
            #('jcitrin', 'run73342_ICRH_EFTM_kplassfit_norot_2'), # 73342
    ]:
        ref_dir = '/common/cmg/{!s}/jetto/runs/{!s}'.format(ref_user, ref_run_name)
        ref_ex_path = os.path.join(ref_dir, ext_filename)

        base_name = re.sub('^' + run_prefix, '', ref_run_name)
        run_suff = '_NN'

        exe_owner = 'kplass'
        exe_name = 'v190503_77ccfb60'
        now = datetime.datetime.now()

        dry_run = False
        scan_list = product(*set_namelist_switches_combo.values())
        scan_names = list(set_namelist_switches_combo.keys())
        start_ix = 0
        for ii, scan_values in enumerate(scan_list):
            for scan_name, scan_value in zip(scan_names, scan_values):
                print(ii + start_ix, scan_name, scan_value)
            identifier = '_'.join(['{!s}{!s}'.format(tiny_name[name], val) for name, val in zip(list(set_namelist_switches_combo.keys())[1:], scan_values[1:])])
            run_name = run_prefix + base_name + run_suff + '_' + identifier
            set_namelist_switches = OrderedDict(zip(scan_names, scan_values))
            run_dir = os.path.join(runs_dir, run_name)
            if not dry_run:
                prep_dir(run_dir)
                copy_file(ref_dir, run_dir, ex_filename)
                copy_file(ref_dir, run_dir, ext_filename)
                #copy_file(ref_dir, run_dir, sin_filename)
                #copy_file(ref_dir, run_dir, sgrid_filename)
                rjettov_path = os.path.join('/home', 'kplass', 'jetto/source', 'jetto-karel', 'sh', rjettov_name)
                run_rjettov_path = os.path.join(run_dir, rjettov_name)
                shutil.copyfile(rjettov_path, run_rjettov_path)
                st = os.stat(run_rjettov_path)
                os.chmod(run_rjettov_path, st.st_mode | stat.S_IEXEC)
                create_llcmd(ref_dir, run_dir, exe_name, exe_owner)
                create_jetto_in(ref_dir, run_dir, now, set_namelist_switches=set_namelist_switches)
                create_jetto_jset(ref_dir, run_dir, now, ref_ex_path, exe_name, exe_owner, set_namelist_switches=set_namelist_switches)
